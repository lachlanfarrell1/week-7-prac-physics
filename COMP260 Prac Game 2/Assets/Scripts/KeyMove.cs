﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class KeyMove : MonoBehaviour {

    private Rigidbody rb;
    private Transform transform;                                    //Player Transform

	void Start () {
        rb.GetComponent<Rigidbody>();                               //Player RigidBody
        rb.useGravity = false;
	}
	

    public float force = 20f;
    public float speed = 10f;

    void FixedUpdate()
    {
        float moveHoriz = Input.GetAxis("Horizontal");
        float moveVert = Input.GetAxis("Vertical");

        transform.Translate(moveHoriz, moveVert, 0);
    }
}
